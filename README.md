simplegui
=========

A Library for easier creation of simple dialogs.

For when you don't need a full blown GUI for your application, just a few small inputs. There are several input types, and each can be left blank or pre-filled with a default value.

Tested in Python 3.9, but should work on all 3.x versions. See source for input options.

**Example:**
```python
import simplegui


msg = (
	"A simple demo dialog. Here are a few options"
	" for asking users (or yourself) for interactive input."
)

d = simplegui.simplegui.Dialog(msg, title="Demo dialog")
d.add_dropdown(
	label="Look at this dropdown:",
	values=["Sweet dropdown", "I've seen better"],
	defaultValue="Sweet dropdown",
)
d.add_text_field(label="Your favorite color:")

d.add_separator()  #separate sections without a label
d.add_radio_buttons(["a", "b", "c"], label="favorite letter")
d.add_checkbox(label="You like?")

d.add_separator("Separate sections with a label")
# Alternatively, don't define any buttons and the dialog will simply use
# `Cancel` and `Ok`
d.add_buttons(
	["More options...", "Stop", "Process"],
	cancelButton="Stop",
	okButton="Process",
)
user_input = d.display()

# If the user doesn't change any values and uses the `Ok button`, this is the
# value returned:
assert user_input == {'values': ['Sweet dropdown', '', 'a', False], 'button returned': 'Process'}
```

Screenshot of the produced dialog:

![Basic dialog example](docs/img/basic_demo.png "Basic dialog example")


Caveats:

This was built to scratch a very specific itch, and I do not typically make updates unless necessary. The code went 9 years between updates, and that was to port it from Python 2.7 to Python 3.9. Feel free to make your own updates. Or, open an issue. Maybe you'll get lucky.
