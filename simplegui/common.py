from tkinter import StringVar, DoubleVar, IntVar, BooleanVar


def get_value_variable(values):
	"""
	Gets the type of variable needed for Tkinter widgets.

	Will return the lowest common denominator variable for a value or list of
	values. For instance, if you have a list with both Strings and Integers,
	simplegui cannot return an IntVar. If the user choose a String, there will
	be an error.

	Args:
			values (Mixed): The value(s) where you want the lowest common
			denominator variable type.

	Returns:
			Tkinter Variable
	"""
	if not isinstance(values, list):
		values = [values]

	valueTypes = [type(value) for value in values]

	if str in valueTypes:
		return StringVar()
	elif float in valueTypes:
		return DoubleVar()
	elif int in valueTypes:
		return IntVar()
	elif bool in valueTypes:
		return BooleanVar()
	else:
		return StringVar()
